const express = require("express");
const {
    getAllTasks,
    insertTasks,
    deleteTask,
    updateTask,
    getSingleTasks,
    getSingleTasksComplete,
} = require("../controllers/taskController");

const router = express.Router();

router.get("/", getAllTasks);

// Create a route for getting a specific task.
router.get("/:id", getSingleTasks);

// Create a route for changing the status of a task to complete.
router.put("/:id/complete", getSingleTasksComplete);

router.post("/insert", insertTasks);

router.delete("/delete/:id", deleteTask);

router.put("/update/:id", updateTask);

module.exports = router;
