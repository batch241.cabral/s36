const Task = require("../models/task");

const getAllTasks = (req, res) => {
    return Task.find({})
        .then((result) => {
            return result;
        })
        .then((resultFromController) => res.send(resultFromController));
};

const insertTasks = (req, res) => {
    let requestBody = req.body;
    let newTask = new Task({
        name: requestBody.name,
    });
    return newTask
        .save()
        .then((task, error) => {
            if (error) {
                return false;
            } else {
                return task;
            }
        })
        .then((resultFromController) => res.send(resultFromController));
};

const deleteTask = (req, res) => {
    const taskId = req.params.id;
    return Task.findByIdAndDelete(taskId)
        .then((result) => {
            if (!result) {
                return res.status(404).send("Task not found.");
            }
            return res.status(200).send("Task deleted successfully.");
        })
        .catch((error) => res.status(500).send({ error: error.message }));
};

const updateTask = (req, res) => {
    let requestBody = req.body;
    let taskID = req.params.id;
    try {
        Task.findById(taskID).then((result) => {
            result.name = requestBody.name;
            return result.save().then((saveUpdatedTask, saveErr) => {
                if (saveErr) {
                    return res.status(400).send("Error");
                } else {
                    return res.status(200).send(saveUpdatedTask);
                }
            });
        });
    } catch (error) {
        res.status(500).send({ error: error.message });
    }
};

//Create a controller function for retrieving a specific task.
const getSingleTasks = (req, res) => {
    let taskID = req.params.id;
    Task.findById(taskID)
        .then((result) => {
            if (result) {
                return result;
            }
        })
        .then((resultFromController) => res.send(resultFromController));
};

//Create a controller function for changing the status of a task to complete.
const getSingleTasksComplete = (req, res) => {
    let requestBody = req.body;
    let taskID = req.params.id;
    Task.findById(taskID).then((result) => {
        result.status = requestBody.status;
        return result.save().then((saveResult, err) => {
            if (err) {
                return res.status(400).send("Error");
            } else {
                return res.status(200).send(saveResult);
            }
        });
    });
};

// const updateTask = async (req, res) => {
//     try {
//         let requestBody = req.body;
//         let taskID = req.params.id;
//         let result = await Task.findById(taskID);
//         result.name = requestBody.name;
//         let saveUpdatedTask = await result.save();
//         return res.send(saveUpdatedTask);
//     } catch (error) {
//         return res.status(400).send(error);
//     }
// };

// const updateTask = async (req, res) => {
//     try {
//         let requestBody = req.body;
//         let taskID = req.params.id;
//         let task = await Task.findById(taskID);
//         if (!task) {
//             return res.status(404).send("Task not found");
//         }
//         task.name = requestBody.name;
//         task = await task.save();
//         return res.status(200).send(task);
//     } catch (error) {
//         return res.status(500).send({ error: error.message });
//     }
// };

module.exports = {
    getAllTasks: getAllTasks,
    insertTasks: insertTasks,
    deleteTask: deleteTask,
    updateTask: updateTask,
    getSingleTasks: getSingleTasks,
    getSingleTasksComplete: getSingleTasksComplete,
};
