const express = require("express");
const mongoose = require("mongoose");

const taskRoutes = require("./routes/taskRoutes");

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
mongoose.set("strictQuery", false);
mongoose.connect(
    "mongodb+srv://admin123:admin123@wdc028-course-booking.rtpbcvq.mongodb.net/s36Discussion?retryWrites=true&w=majority",
    { useNewUrlParser: true, useUnifiedTopology: true }
);

let db = mongoose.connection;

db.on("err", console.error.bind(console, "Error on connecting"));
db.once("open", () => console.log("Connected"));

app.use("/tasks", taskRoutes);

const port = process.env.PORT || 4000;
app.listen(port, () => console.log(`Listening on port ${port}`));
